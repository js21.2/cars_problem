function problem3(inventory){
    if( inventory.length == 0){
        return null;
    }
    let models = new Array();
    for ( let i = 0; i < inventory.length ; i++ ){
        let model = inventory[i].car_model.trim().toUpperCase();
        models.push(model)
    }
    
    const sortedModels = models.sort();
    const modelsSet = new Set(sortedModels);
    //console.log(modelsSet.size)
    return modelsSet;
}

module.exports = problem3;

