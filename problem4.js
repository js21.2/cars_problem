function problem4(inventory){
    if( inventory.length == 0){
        return null;
    }
    let years = new Array();
    for ( let i = 0; i < inventory.length ; i++ ){
        let year = inventory[i].car_year ;
        years.push(year);
    }
    
    const sortedData = years.sort();
    return new Set(sortedData);
}

module.exports = problem4;

