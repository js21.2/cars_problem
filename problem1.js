//const inventory = require('./cars_problem')

function problem1(inventory, id) {
    const index = getIndex(id, inventory);
    if (index == -1) {
        console.log(`sorry.. no car data is found with the given id: ${id}, please try again`);
    }
    else {
        const newLocal = `Car ${id} is a ${inventory[index].car_year} ${inventory[index].car_make} ${inventory[index].car_model}`;
        console.log(newLocal)
    }
}

module.exports = problem1


function getIndex(id, inventory) {
    let pos = -1;
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].id == id) {
            pos = i;
            break;
        }
    }
    return pos;
}