function problem5(inventory){
    if( inventory.length == 0){
        return null;
    }
    let carsBefore2000 = new Array();
    for ( let i = 0; i < inventory.length ; i++ ){
        let year = inventory[i].car_year ;
        if(year< 2000){
            carsBefore2000.push(inventory[i]);
        }
    }
    
    return carsBefore2000;
    
}

module.exports = problem5;
