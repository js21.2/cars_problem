const inventory = require('../cars_problem');
const problem5 = require('../problem5');
const result = problem5(inventory);

if(result == null){
    console.log('There are no cars in the inventory')
}
else{
    console.log(`${result.length} cars were made before the year 2000 `)
}