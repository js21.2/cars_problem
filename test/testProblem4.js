const inventory = require('../cars_problem');
const problem4 = require('../problem4');
const result = problem4(inventory);

if(result == null){
    console.log('There are no cars in the inventory to dispay the models')
}
else{
    for (const year of result){
        console.log(year)
    }
}