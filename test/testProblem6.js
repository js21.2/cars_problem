const inventory = require('../cars_problem');
const problem6 = require('../problem6');
const result = problem6(inventory);

if(result == null){
    console.log('There are no cars in the inventory');
}
else{
    console.log(`BMWand Audi car details:`);
    console.log(JSON.stringify([...result]));

}