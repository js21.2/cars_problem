const inventory = require('../cars_problem');
const problem3 = require('../problem3');
const result = problem3(inventory);

if(result == null){
    console.log('There are no cars in the inventory to dispay the models')
}
else{
    for (const model of result){
        console.log(model)
    }
}