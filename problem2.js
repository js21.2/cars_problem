function problem2(inventory) {
    const index = getLastIndex(inventory);
    if (index == -1) {
        console.log(`sorry.. no cars are available in the data. please add some cars data`);
    }
    else {
        const newLocal = `Last car is a ${inventory[index].car_make} ${inventory[index].car_model}`;
        console.log(newLocal);
    }
}

module.exports = problem2;


function getLastIndex(inventory) {
    pos = -1
    max = -1;
    for (let i = 0; i < inventory.length; i++) {
        if (max < inventory[i].id) {
            max = inventory[i].id;
            pos = i;
        }
    }
    return pos;
}