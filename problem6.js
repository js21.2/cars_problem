function problem6(inventory){
    if( inventory.length == 0){
        return null;
    }
    let bmwAduiCars = new Array();
    for ( let i = 0; i < inventory.length ; i++ ){
        let make = inventory[i].car_make .trim().toUpperCase();
        if(make == 'BMW' || make == 'AUDI'){
            bmwAduiCars.push(inventory[i]);
            //console.log('pesent car list:'  + bmwAduiCars)
        }
    }
    
    return bmwAduiCars;
    
}

module.exports = problem6;
